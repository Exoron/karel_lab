from textx import metamodel_from_file
from karel_robot.run import *

karel_mm = metamodel_from_file('karel.tx')

karel_model = karel_mm.model_from_file('maze.karel')

def is_(o, name):
    return o.__class__.__name__ == name

class Robot:
    funcs = dict() # (params, body)
    contexts = [] # name -> value

    def process_command(self, command):
        if command == 'exit':
            exit()
        elif command == 'move':
            move()
        elif is_(command, 'Turn'):
            if command.where == 'left':
                turn_left()
            if command.where == 'right':
                turn_right()
        elif is_(command, 'Beeper'):
            if command.action == 'pick':
                pick_beeper()
            if command.action == 'put':
                put_beeper()
        elif is_(command, 'Call'):
            self.process_call(command)
            
    def process_call(self, command):
        name = command.name
        params = command.param
        
        (names, body) = self.funcs[name]
        assert(len(params) == len(names))

        ctx = {names[i]: self.process_expression(params[i]) for i in range(len(names))}
        self.contexts.append(ctx)

        self.process_statements(body)

        self.contexts.pop()


    def process_expression(self, e):
        value = False
        for part in e.parts:
            value = value or self.process_conjunction(part)
        return value

    def process_conjunction(self, e):
        value = True
        for part in e.parts:
            value = value and self.process_term(part)
        return value

    def process_term(self, e):
        if is_(e, 'Not'):
            return self.process_not(e)
        else:
            return self.process_singleton(e)

    def process_not(self, e):
        return not self.process_singleton(e.content)

    def process_singleton(self, e):
        if is_(e, 'Scope'):
            return self.process_scope(e)
        elif is_(e, 'Var'):
            return self.contexts[-1][e.name]
        else:
            return self.process_bool_const(e)

    def process_scope(self, e):
        return e.content

    def process_bool_const(self, e):
        if e == 'north':
            return facing_north()
        if e == 'south':
            return facing_south()
        if e == 'west':
            return facing_west()
        if e == 'east':
            return facing_east()

        if e == 'front_is_treasure':
            return front_is_treasure()
        if e == 'front_is_blocked':
            return front_is_blocked()
        if e == 'is_beeper':
            return beeper_is_present()

        raise Exception('No such bool const')


    def process_statements(self, cs):
        for c in cs:
            self.process_statement(c)
       

    def process_statement(self, s):
        self.process_command(s)

        if is_(s, 'StatementWhile'):
            while self.process_expression(s.cond):
                self.process_statements(s.commands)

        
        if is_(s, 'StatementIf'):
            if self.process_expression(s.cond):
                self.process_statements(s.commands)
            else:
                self.process_statements(s.else_commands)

        if is_(s, 'Procedure'):
            self.process_procedure_definition(s)

    def process_procedure_definition(self, s):
        name = s.name
        params = s.param
        body = s.commands
        self.funcs[name] = (params, body)

    def interpret(self, model):
        for c in model.commands:
            self.process_statement(c)

robot = Robot()
robot.interpret(karel_model)
